# Examen AWE

## Contexto

Una comunidad de yoguis tibetanos quiere tener una web con contenidos relevantes sobre su disciplina, así como algunos de sus eventos y horarios. Afortunadamente, han encomendado el desarrollo de la web al equipo en el que trabajas.


### Consejo

Lee con atención cada uno de los enunciados antes de comenzar el ejercicio.


### Importante

* Para evaluar los conocimientos de Git, el alumno deberá entregar el historial de comandos utilizados durante el examen usando el fichero `history.txt`. Para facilitar esta tarea, se recomienda ejecutar la siguiente línea en la terminal que vaya a usarse para llevar a cabo el examen.

    ```
        HISTTIMEFORMAT="%d/%m/%y %T "
    ```

* El documento `index.html` contiene una etiqueta `meta` en la que deberás indicar tu nombre completo antes de comenzar a trabajar en el mismo. Una vez comiences la prueba, no deberás modificar el contenido de esta etiqueta bajo ningún concepto.


## Enunciados

1. __(1 punto)__ En Gitlab, haz un fork de este repositorio. Clona tu fork en local y crea una nueva rama con el formato `apellido1-apellido2-nombre` a partir de `peticiones-yoguis`.

2. __(2 puntos)__ Tras indicar tu nombre en el campo reservado en `index.html`, aparecerá un test con diez afirmaciones sobre Scrum. Indica si estas afirmaciones son verdaderas o falsas. Usando el botón _Guardar respuestas_ se generará un fichero `respuestas.json` que deberás incluir en el repositorio. 

3. __(4 puntos)__ El documento `index.html` contiene imágenes proporcionadas por los yoguis, las cuáles pertenecen a la clase `_2html`. Como habéis repartido el trabajo usando Scrum, tu equipo te ha asignado dos de estas imágenes, como puedes ver al abrir la web en el navegador. Modifica `index.html` para traducir al lenguaje HTML el contenido de dichas imágenes.

    * Asegúrate de realizar `git commit` cuando hagas cambios considerables (al menos uno por cada imagen).

4. __(1 punto)__ Uno de los desarrolladores de tu equipo Scrum está trabajando en la rama `estilos-css` a fin de implementar una serie de estilos que mejoran la visualización de la web. Utilizando la CLI de Git, aplica a tu rama los cambios del compañero.

    * Asegúrate de que los cambios aplicados se guardan en tu repositorio.

5. __(1 punto)__ Tras mezclar los cambios de la rama `estilos-css` con tu trabajo, te percatas de que algunos estilos no se aplican correctamente. Tal y como sospechabas, tu compañero olvidó hacer commit de algunos cambios en el HTML. Corrige las etiquetas necesarias en `index.html` para que los selectores CSS que no tienen efecto actualmente cumplan su función.

    * Asegúrate de que las correcciones se guardan en tu repositorio.

6. __(1 punto)__ Haz push de tu rama de forma que se sincronice con tu fork. A continuación, crea un merge request hacia la rama `main` del repositorio original.

    * Asegúrate de verificar que el documento HTML valida con [el servicio de validación del W3C](https://validator.w3.org/).